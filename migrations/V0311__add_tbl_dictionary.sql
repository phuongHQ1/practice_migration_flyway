CREATE TABLE `tbl_dictionary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(128) DEFAULT NULL,
  `field_name` varchar(45) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;