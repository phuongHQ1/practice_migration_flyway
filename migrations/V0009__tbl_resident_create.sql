CREATE TABLE `tbl_resident` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(32) NULL COMMENT 'SAP profile ID',
  `full_name` VARCHAR(64) NULL,
  `dob` bigint(20) NULL,
  `phone_no` CHAR(12) NULL,
  `id_number` CHAR(16) NULL,
  `gender` INT NULL,
  `relationship` INT NULL,
  PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

  
ALTER TABLE `tbl_user_apartment`
ADD COLUMN `resident_id` INT NULL AFTER `phone_no`,
ADD INDEX `fk_tbl_user_apartment_resident_id_idx` (`resident_id` ASC);
;
ALTER TABLE `tbl_user_apartment`
ADD CONSTRAINT `fk_tbl_user_apartment_resident_id`
  FOREIGN KEY (`resident_id`)
  REFERENCES `tbl_resident` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;