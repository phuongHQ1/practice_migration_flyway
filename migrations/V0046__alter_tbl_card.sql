ALTER TABLE `tbl_card` 
ADD COLUMN `valid_from` BIGINT NULL AFTER `status`,
ADD COLUMN `valid_to` BIGINT NULL AFTER `valid_from`;