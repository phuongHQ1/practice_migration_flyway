ALTER TABLE `tbl_group_permission`
ADD CONSTRAINT `unique_group_permission_id`
UNIQUE (group_id, permission_id);
