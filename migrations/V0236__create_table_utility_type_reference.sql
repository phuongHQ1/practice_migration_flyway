CREATE TABLE `tbl_utility_type_reference`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `utility_id` int(11) NULL,
  `utility_type_id` int(11) NOT NULL,
  `created_date` timestamp NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_utility_type_reference` FOREIGN KEY (`utility_type_id`) REFERENCES `tbl_utility_type` (`id`)
) ENGINE = InnoDB;
