UPDATE tbl_booking_utility bu 
JOIN   tbl_utility u  ON bu.utility_id = u.id 
SET    bu.utility_type_id = u.utility_type_id 
WHERE  bu.utility_type_id IS NULL; 
