CREATE TABLE `tbl_city_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `updated_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT unique_city_permission UNIQUE (city_id, permission_id),
  CONSTRAINT `fk_tbl_city_permission_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `tbl_permission` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_city_permission_city_id` FOREIGN KEY (`city_id`) REFERENCES `tbl_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
