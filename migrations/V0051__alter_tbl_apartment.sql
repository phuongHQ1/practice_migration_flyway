ALTER TABLE `tbl_apartment` 
ADD COLUMN `sap_block_id` INT(11) NULL AFTER `floor_id`,
ADD COLUMN `sap_floor_id` INT(11) NULL AFTER `sap_block_id`;