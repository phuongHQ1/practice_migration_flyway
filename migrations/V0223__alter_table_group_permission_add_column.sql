alter table tbl_group_permission
add column `created_by` varchar(32) default 'admin',
add column `created_date` timestamp default current_timestamp,
add column `created_ymd` int(11);