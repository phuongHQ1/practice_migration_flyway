CREATE TABLE `tbl_group_third_party`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NULL,
  `thirdparty_id` int(11) NULL,
  `created_by` varchar(128) NULL,
  `created_date` timestamp(0) NULL,
  PRIMARY KEY (`id`)
);