ALTER TABLE `tbl_booking_utility`
ADD COLUMN `sap_ticket_id` VARCHAR(32) NULL,
ADD COLUMN `sap_ticket_status` VARCHAR(32) NULL AFTER `sap_ticket_id`;
