CREATE TABLE `tbl_card` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(32) NULL COMMENT 'SAP profile ID',
  `full_name` VARCHAR(64) NULL,
  `dob` bigint(20) NULL,
  `phone_no` CHAR(12) NULL,
  `id_number` CHAR(16) NULL,
  `relationship` INT NULL,
  `card_type` INT NULL,
  `vehicle_type` INT NULL,
  `vehicle_model` VARCHAR(64) NULL,
  `vehicle_number` VARCHAR(16) NULL,
  `vehicle_color` VARCHAR(32) NULL,
  `status` INT NULL,
  `create_date` TIMESTAMP NULL,
  `ymd` INT NULL,
  `resident_id` INT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

 