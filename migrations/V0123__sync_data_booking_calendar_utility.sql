INSERT INTO tbl_booking_calendar_utility(id, status, created_by, created_date, end_time, slot_id, start_time, ymd, booking_ymd, utility_id, start_ymd_hm, end_ymd_hm)
SELECT ANY_VALUE(id),
CASE (SELECT tmp_booking_utility.status FROM tbl_booking_utility tmp_booking_utility 
	WHERE tmp_booking_utility.booking_ymd = booking_ymd 
	AND tmp_booking_utility.booking_slot = booking_slot 
	AND tmp_booking_utility.utility_id = utility_id ORDER BY created_date DESC LIMIT 1) WHEN 1 THEN 1
ELSE 0 END, 
'migration', ANY_VALUE(created_date), ANY_VALUE(booking_slot_end), ANY_VALUE(booking_slot), 
ANY_VALUE(booking_slot_start), ANY_VALUE(ymd), ANY_VALUE(booking_ymd), ANY_VALUE(utility_id), 
ANY_VALUE(CONCAT(booking_ymd, REPLACE(booking_slot_start, ':', '') )),  ANY_VALUE(CONCAT(booking_ymd, REPLACE(booking_slot_end, ':', '') ))  FROM tbl_booking_utility
GROUP BY  booking_ymd,  booking_slot, utility_id;
