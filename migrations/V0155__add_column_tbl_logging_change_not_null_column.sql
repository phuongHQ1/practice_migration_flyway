ALTER TABLE `idhomes_service`.`tbl_logging`
MODIFY COLUMN `client_ip` varchar(128) DEFAULT NULL,
MODIFY COLUMN  `create_user` VARCHAR(20) DEFAULT NULL,
MODIFY COLUMN  `method` varchar(20)  DEFAULT NULL,
MODIFY COLUMN  `response_body` longtext DEFAULT NULL,
MODIFY COLUMN  `uri` varchar(128) DEFAULT NULL;


