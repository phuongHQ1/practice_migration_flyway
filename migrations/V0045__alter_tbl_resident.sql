ALTER TABLE `tbl_resident` 
ADD COLUMN `email` VARCHAR(128) NULL AFTER `relationship`,
ADD COLUMN `address` VARCHAR(256) NULL AFTER `email`,
ADD COLUMN `valid_from` BIGINT NULL AFTER `address`,
ADD COLUMN `valid_to` BIGINT NULL AFTER `valid_from`;
