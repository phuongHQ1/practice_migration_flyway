CREATE TABLE `tbl_utility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `updated_by` timestamp NULL DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `utility_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_utility_city_id_idx` (`city_id`),
  KEY `fk_tbl_utility_type_id_idx` (`utility_type_id`),
  CONSTRAINT `fk_tbl_utility_city_id` FOREIGN KEY (`city_id`) REFERENCES `tbl_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_utility_type_id` FOREIGN KEY (`utility_type_id`) REFERENCES `tbl_utility_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
