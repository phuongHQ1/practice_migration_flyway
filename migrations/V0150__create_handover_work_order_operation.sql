CREATE TABLE `tbl_handover_work_order_operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `operation_wc` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `sap_operation_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `name` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `workorder_id` int(11) NOT NULL,
  `operation_status` varchar(16) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_work_order_operation_work_order_idx` (`workorder_id`),
  CONSTRAINT `fk_tbl_work_order_operation_work_order_idx` FOREIGN KEY (`workorder_id`) REFERENCES `tbl_handover_work_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;