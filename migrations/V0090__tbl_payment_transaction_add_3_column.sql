alter table tbl_payment_transaction add column sap_status_code varchar(16) NULL;
alter table tbl_payment_transaction add column sap_error_code varchar(32) NULL;
alter table tbl_payment_transaction add column sap_error_description varchar(256) NULL;