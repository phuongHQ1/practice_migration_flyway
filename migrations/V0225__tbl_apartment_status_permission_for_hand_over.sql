INSERT INTO tbl_apartment_status_permission(apartment_status_id, permission_id, city_id)
SELECT a.id AS `status`, p.id AS permission, 19 AS city_id FROM tbl_apartment_status a JOIN tbl_permission p
WHERE a.id < 70 AND p.id IN (30, 70, 80, 100, 110, 120, 130, 140, 150, 160) ORDER BY p.id ASC