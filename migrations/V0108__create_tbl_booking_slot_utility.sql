CREATE TABLE `tbl_booking_slot_utility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `canceled_by` varchar(255) DEFAULT NULL,
  `canceled_date` datetime DEFAULT NULL,
  `canceled_reason` varchar(255) DEFAULT NULL,
  `is_used` int(11) DEFAULT NULL,
  `status` int(4) DEFAULT NULL,
  `booking_calendar_utility_id` int(11) DEFAULT NULL,
  `booking_utility_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_booking_slot_utility_booking_calendar_utility_id_idx` (`booking_calendar_utility_id`),
  KEY `fk_tbl_booking_slot_utility_booking_utility_id_idx` (`booking_utility_id`),
  CONSTRAINT `fk_tbl_booking_slot_utility_booking_calendar_utility_id_idx` FOREIGN KEY (`booking_calendar_utility_id`) REFERENCES `tbl_booking_calendar_utility` (`id`),
  CONSTRAINT `fk_tbl_booking_slot_utility_booking_utility_id_idx` FOREIGN KEY (`booking_utility_id`) REFERENCES `tbl_booking_utility` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
