ALTER TABLE `tbl_folio`
ADD COLUMN `owed_amount` VARCHAR(32) NULL AFTER `folio_detail_id`,
ADD COLUMN `paid_amount` VARCHAR(32) NULL AFTER `owed_amount`;
