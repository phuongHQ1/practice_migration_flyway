CREATE TABLE `tbl_utility_block` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`city_id` int(3) NOT NULL,
`utility_type_id` int(3) NOT NULL,
`apartment_id` int(3) NOT NULL,
`reason` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
`start_time` timestamp NULL DEFAULT NULL,
`end_time` timestamp NULL DEFAULT NULL,
`portal_status` int(11) DEFAULT NULL,
`portal_comment` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
`portal_updated_date` timestamp NULL DEFAULT NULL,
`portal_updated_by` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
`portal_created_date` timestamp NULL DEFAULT NULL,
PRIMARY KEY (`id`),
KEY `fk_tbl_utility_block_city_id_idx` (`city_id`),
KEY `fk_tbl_utility_block_utility_id_idx` (`utility_type_id`),
KEY `fk_tbl_utility_block_apartment_id_idx` (`apartment_id`),
CONSTRAINT `fk_tbl_utility_block_apartment_id` FOREIGN KEY (`apartment_id`) REFERENCES `tbl_apartment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT `fk_tbl_utility_block_city_id` FOREIGN KEY (`city_id`) REFERENCES `tbl_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT `fk_tbl_utility_block_utility_type_id` FOREIGN KEY (`utility_type_id`) REFERENCES `tbl_utility_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

