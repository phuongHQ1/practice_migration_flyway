CREATE TABLE `tbl_third_party_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `deep_link` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `icon_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` int(3) DEFAULT '1' COMMENT '1 for active, 0 for inactive',
  `title` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
