CREATE TABLE tbl_mobile_version (
    id int not null AUTO_INCREMENT,
    client_type int not null,
    version varchar(100) not null,
    PRIMARY KEY (id),
    UNIQUE (client_type, version)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE tbl_feature_mobile_version (
    id int not null AUTO_INCREMENT,
    version_id int not null,
    feature_id int not null,
    PRIMARY KEY (id),
    CONSTRAINT unique_version_feature UNIQUE (version_id, feature_id),
    CONSTRAINT foreign_key_version FOREIGN KEY (version_id) REFERENCES tbl_mobile_version (id) ON DELETE CASCADE,
    CONSTRAINT foreign_key_feature FOREIGN KEY (feature_id) REFERENCES tbl_permission (id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;