update tbl_booking_calendar_utility set start_time = CONCAT(0,start_time) where created_by  = 'migration' and  LENGTH(start_time) = 4;
update tbl_booking_calendar_utility set end_time = CONCAT(0,end_time) where created_by  = 'migration' and  LENGTH(end_time) = 4;
update tbl_booking_calendar_utility set start_ymd_hm = CONCAT(booking_ymd, REPLACE(start_time, ':', '') ) where created_by  = 'migration';
update tbl_booking_calendar_utility set end_ymd_hm = CONCAT(booking_ymd, REPLACE(end_time, ':', '') ) where created_by  = 'migration';