ALTER TABLE `idhomes_service`.`tbl_handover_apartment_delivery`
DROP COLUMN `type`,
DROP INDEX `constr_ID` ,
ADD UNIQUE INDEX `constr_ID` (`phone_no` ASC, `apartment_id` ASC);
