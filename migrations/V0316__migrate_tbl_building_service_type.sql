insert into tbl_building_service_type(`building_id`, `service_type_id`, `created_date`)
select b.`id` `building_id`, cs.`service_type_id`, now() from tbl_building b
join (select distinct d.`service_type_id`, d.`city_id` from tbl_city_service_type d) cs on b.city_id = cs.city_id;