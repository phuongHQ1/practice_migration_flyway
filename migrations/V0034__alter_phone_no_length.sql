ALTER TABLE `tbl_booking_online`
CHANGE COLUMN `phone_no` `phone_no` VARCHAR(128) NULL DEFAULT NULL ;

ALTER TABLE `tbl_booking_utility`
CHANGE COLUMN `phone_no` `phone_no` VARCHAR(128) NULL DEFAULT NULL ;

ALTER TABLE `tbl_card`
CHANGE COLUMN `phone_no` `phone_no` VARCHAR(128) NULL DEFAULT NULL ;

ALTER TABLE `tbl_user_apartment`
CHANGE COLUMN `phone_no` `phone_no` VARCHAR(128) NULL DEFAULT NULL ;

ALTER TABLE `tbl_resident`
CHANGE COLUMN `phone_no` `phone_no` VARCHAR(128) NULL DEFAULT NULL ;

ALTER TABLE `tbl_resident`
CHANGE COLUMN `full_name` `full_name` VARCHAR(128) NULL DEFAULT NULL ;

ALTER TABLE `tbl_resident`
CHANGE COLUMN `id_number` `id_number` VARCHAR(128) NULL DEFAULT NULL ;

ALTER TABLE `tbl_resident`
CHANGE COLUMN `dob` `dob` VARCHAR(128) NULL DEFAULT NULL ;

ALTER TABLE `tbl_user`
CHANGE COLUMN `id_number` `id_number` VARCHAR(128) NULL DEFAULT NULL ;

ALTER TABLE `tbl_user` DROP COLUMN `phone_no`;
