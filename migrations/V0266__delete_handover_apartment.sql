START TRANSACTION;

DELETE FROM `tbl_handover_apartment_delivery` WHERE `apartment_id` = 77691;

DELETE FROM `tbl_handover_booking` WHERE `apartment_id` = 77691;

DELETE FROM `tbl_handover_feedback` WHERE `apartment_id` = 77691;

DELETE FROM `tbl_handover_notification` WHERE `workorder_id` in ( SELECT id FROM `tbl_handover_work_order` WHERE `apartment_id` = 77691) ;

DELETE FROM `tbl_handover_work_order` WHERE `apartment_id` = 77691;

DELETE FROM `tbl_handover_apartment` WHERE `apartment_id` = 77691;

DELETE FROM `tbl_referrer` WHERE `apartment_id` = 77691;

DELETE FROM `tbl_user_apartment` WHERE `apartment_id` = 77691;

DELETE FROM `tbl_apartment` WHERE `id` = 77691;

COMMIT;