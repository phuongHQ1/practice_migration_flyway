CREATE TABLE `tbl_handover_apartment_delivery` (
  `id` int(11) NOT NULL,
  `phone_no` varchar(128) NOT NULL,
  `apartment_id` int(11) NOT NULL,
  `type` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `constr_ID` (`phone_no`,`apartment_id`,`type`),
  KEY `fk_tbl_handover_user_apartment_idx` (`apartment_id`),
  CONSTRAINT `fk_tbl_handover_user_apartment` FOREIGN KEY (`apartment_id`) REFERENCES `tbl_apartment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;