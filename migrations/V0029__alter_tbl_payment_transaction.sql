ALTER TABLE `tbl_payment_transaction`
ADD COLUMN `debit_note_info` VARCHAR(128) NULL AFTER `ymd`,
ADD COLUMN `apartment_id` INT NULL AFTER `debit_note_info`,
ADD COLUMN `city_id` INT NULL AFTER `apartment_id`,
ADD INDEX `fk_tbl_payment_transaction_apartment_id_idx` (`apartment_id` ASC);

ALTER TABLE `tbl_payment_transaction`
ADD CONSTRAINT `fk_tbl_payment_transaction_apartment_id`
  FOREIGN KEY (`apartment_id`)
  REFERENCES `tbl_apartment` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;