CREATE TABLE `tbl_city_other_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '1',
  `city_id` int(11) DEFAULT NULL,
  `other_service_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_city_other_service_other_service_id_idx` (`other_service_id`),
  KEY `FKe46q592dlkq45urwlvtbxd5pd` (`city_id`),
  CONSTRAINT `fk_tbl_city_other_service_city_id` FOREIGN KEY (`city_id`) REFERENCES `tbl_city` (`id`),
  CONSTRAINT `fk_tbl_city_other_service_other_service_id` FOREIGN KEY (`other_service_id`) REFERENCES `tbl_other_service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
