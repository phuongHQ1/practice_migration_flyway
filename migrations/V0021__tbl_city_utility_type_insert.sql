INSERT INTO `tbl_city_utility_type` (`id`, `city_id`, `utility_type_id`, `created_date`, `created_by`, `status`)
VALUES ('1', '1', '1', NOW(), 'admin', '1');
INSERT INTO `tbl_city_utility_type` (`id`, `city_id`, `utility_type_id`, `created_date`, `created_by`, `status`)
VALUES ('2', '1', '2', NOW(), 'admin', '1');
INSERT INTO `tbl_city_utility_type` (`id`, `city_id`, `utility_type_id`, `created_date`, `created_by`, `status`)
VALUES ('3', '1', '3', NOW(), 'admin', '1');
INSERT INTO `tbl_city_utility_type` (`id`, `city_id`, `utility_type_id`, `created_date`, `created_by`, `status`)
VALUES ('4', '1', '4', NOW(), 'admin', '1');
INSERT INTO `tbl_city_utility_type` (`id`, `city_id`, `utility_type_id`, `created_date`, `created_by`, `status`)
VALUES ('5', '1', '5', NOW(), 'admin', '1');
INSERT INTO `tbl_city_utility_type` (`id`, `city_id`, `utility_type_id`, `created_date`, `created_by`, `status`)
VALUES ('6', '2', '1', NOW(), 'admin', '1');
INSERT INTO `tbl_city_utility_type` (`id`, `city_id`, `utility_type_id`, `created_date`, `created_by`, `status`)
VALUES ('7', '2', '2', NOW(), 'admin', '1');
INSERT INTO `tbl_city_utility_type` (`id`, `city_id`, `utility_type_id`, `created_date`, `created_by`, `status`)
VALUES ('8', '2', '3', NOW(), 'admin', '1');
INSERT INTO `tbl_city_utility_type` (`id`, `city_id`, `utility_type_id`, `created_date`, `created_by`, `status`)
VALUES ('9', '2', '4', NOW(), 'admin', '1');
INSERT INTO `tbl_city_utility_type` (`id`, `city_id`, `utility_type_id`, `created_date`, `created_by`, `status`)
VALUES ('10', '2', '5', NOW(), 'admin', '1');

UPDATE `tbl_city_utility_type` SET `booking_limit_day` = '7', `open_hour` = '8', `close_hour` = '22', `slot_hour` = '1', `limit_per_day` = '2', `allow_multi_per_slot` = '0', `limit_adult` = '10', `limit_child` = '5' WHERE (`id` = '1');
UPDATE `tbl_city_utility_type` SET `booking_limit_day` = '7', `open_hour` = '8', `close_hour` = '22', `slot_hour` = '1', `limit_per_day` = '2', `allow_multi_per_slot` = '0', `limit_adult` = '10', `limit_child` = '5' WHERE (`id` = '2');
UPDATE `tbl_city_utility_type` SET `booking_limit_day` = '7', `open_hour` = '8', `close_hour` = '22', `slot_hour` = '1', `limit_per_day` = '2', `allow_multi_per_slot` = '0', `limit_adult` = '10', `limit_child` = '5' WHERE (`id` = '3');
UPDATE `tbl_city_utility_type` SET `booking_limit_day` = '7', `open_hour` = '8', `close_hour` = '22', `slot_hour` = '1', `limit_per_day` = '2', `allow_multi_per_slot` = '0', `limit_adult` = '10', `limit_child` = '5' WHERE (`id` = '4');
UPDATE `tbl_city_utility_type` SET `booking_limit_day` = '7', `open_hour` = '8', `close_hour` = '22', `slot_hour` = '1', `limit_per_day` = '2', `allow_multi_per_slot` = '0', `limit_adult` = '10', `limit_child` = '5' WHERE (`id` = '5');
UPDATE `tbl_city_utility_type` SET `booking_limit_day` = '7', `open_hour` = '8', `close_hour` = '22', `slot_hour` = '1', `limit_per_day` = '2', `allow_multi_per_slot` = '0', `limit_adult` = '10', `limit_child` = '5' WHERE (`id` = '6');
UPDATE `tbl_city_utility_type` SET `booking_limit_day` = '7', `open_hour` = '8', `close_hour` = '22', `slot_hour` = '1', `limit_per_day` = '2', `allow_multi_per_slot` = '0', `limit_adult` = '10', `limit_child` = '5' WHERE (`id` = '7');
UPDATE `tbl_city_utility_type` SET `booking_limit_day` = '7', `open_hour` = '8', `close_hour` = '22', `slot_hour` = '1', `limit_per_day` = '2', `allow_multi_per_slot` = '0', `limit_adult` = '10', `limit_child` = '5' WHERE (`id` = '8');
UPDATE `tbl_city_utility_type` SET `booking_limit_day` = '7', `open_hour` = '8', `close_hour` = '22', `slot_hour` = '1', `limit_per_day` = '2', `allow_multi_per_slot` = '0', `limit_adult` = '10', `limit_child` = '5' WHERE (`id` = '9');
UPDATE `tbl_city_utility_type` SET `booking_limit_day` = '7', `open_hour` = '8', `close_hour` = '22', `slot_hour` = '1', `limit_per_day` = '2', `allow_multi_per_slot` = '0', `limit_adult` = '10', `limit_child` = '5' WHERE (`id` = '10');


UPDATE `tbl_city_utility_type` SET `fee` = 'Theo quy định của BQL' WHERE (`id` = '1');
UPDATE `tbl_city_utility_type` SET `fee` = 'Theo quy định của BQL' WHERE (`id` = '2');
UPDATE `tbl_city_utility_type` SET `fee` = 'Theo quy định của BQL' WHERE (`id` = '3');
UPDATE `tbl_city_utility_type` SET `fee` = 'Theo quy định của BQL' WHERE (`id` = '4');
UPDATE `tbl_city_utility_type` SET `fee` = 'Theo quy định của BQL' WHERE (`id` = '5');
UPDATE `tbl_city_utility_type` SET `fee` = 'Theo quy định của BQL' WHERE (`id` = '6');
UPDATE `tbl_city_utility_type` SET `fee` = 'Theo quy định của BQL' WHERE (`id` = '7');
UPDATE `tbl_city_utility_type` SET `fee` = 'Theo quy định của BQL' WHERE (`id` = '8');
UPDATE `tbl_city_utility_type` SET `fee` = 'Theo quy định của BQL' WHERE (`id` = '9');
UPDATE `tbl_city_utility_type` SET `fee` = 'Theo quy định của BQL' WHERE (`id` = '10');
