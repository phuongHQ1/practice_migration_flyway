CREATE TABLE `tbl_handover_notification` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `workorder_id` INT(11) NOT NULL,
  `notification_id` varchar(32) NOT NULL,
  `status` INT(2) NULL,
  `created_by` varchar(128) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_workoder_id` FOREIGN KEY (`workorder_id`) REFERENCES `tbl_handover_work_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;