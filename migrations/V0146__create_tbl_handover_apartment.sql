CREATE TABLE `tbl_handover_apartment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `apartment_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `apartment_id_UNIQUE` (`apartment_id`),
  KEY `fk_tbl_apartment_handover_apartment_id_idx` (`apartment_id`),
  KEY `fk_tbl_apartment_handover_city_id_idx` (`city_id`),
  CONSTRAINT `fk_tbl_apartment_handover_apartment_id_idx` FOREIGN KEY (`apartment_id`) REFERENCES `tbl_apartment` (`id`),
  CONSTRAINT `tbl_handover_apartment_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `tbl_city` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;