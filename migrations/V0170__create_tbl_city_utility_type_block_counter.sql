CREATE TABLE `tbl_city_utility_type_block_counter` (
  id int(11) NOT NULL AUTO_INCREMENT,
  apartment_id int(11)  NOT NULL,
  city_utility_type_block_id int(11) NOT NULL,
  counter int(11) DEFAULT 0,
  updated_date datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
