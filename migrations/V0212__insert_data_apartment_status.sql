INSERT INTO tbl_apartment_status(id, code, description)VALUES(10, 'CRTD', 'Chưa nghiệm thu');
INSERT INTO tbl_apartment_status(id, code, description)VALUES(20, 'INST', 'Đã nghiệm thu, chưa đủ dk tài chính');
INSERT INTO tbl_apartment_status(id, code, description)VALUES(30, 'FINA', 'Chưa nghiệm thu, đủ dk tài chính');
INSERT INTO tbl_apartment_status(id, code, description)VALUES(40, 'DONE', 'Sãn sàng bàn giao');
INSERT INTO tbl_apartment_status(id, code, description)VALUES(50, 'PEND', 'Đang bàn giao');
INSERT INTO tbl_apartment_status(id, code, description)VALUES(60, 'ACTI', 'Đã bàn giao');
INSERT INTO tbl_apartment_status(id, code, description)VALUES(70, 'USED', 'Đang khai thác');
INSERT INTO tbl_apartment_status(id, code, description)VALUES(80, 'EXPI', 'Hết hiệu lực');
INSERT INTO tbl_apartment_status(id, code, description)VALUES(90, 'DEMA', 'Huỷ bỏ');