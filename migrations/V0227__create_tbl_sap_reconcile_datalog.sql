CREATE TABLE `tbl_sap_reconcile_datalog` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `job_name` VARCHAR(128) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_bin' NOT NULL,
  `created_date` VARCHAR(128) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_bin' NULL,
  `created_ymd` VARCHAR(32) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_bin' NOT NULL,
  `start_time` BIGINT(20) NULL,
  `end_time` BIGINT(20) NULL,
  `data` LONGTEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_bin' NULL,
  PRIMARY KEY (`id`));