CREATE TABLE `tbl_user` (
  `id` char(16) NOT NULL COMMENT 'VinID user profile ID',
  `phone_no` char(12) DEFAULT NULL COMMENT 'VinID user profile - Phone number',
  `id_number` char(12) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1: Active\n2: Inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


create TABLE `tbl_group` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` CHAR(32) NULL,
  `description` VARCHAR(256) NULL,
  `status` INT NULL COMMENT '1: Active\n0: Inactive',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


create TABLE `tbl_permission` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` CHAR(32) NULL,
  `description` VARCHAR(256) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


CREATE TABLE `tbl_group_permission` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `group_id` INT NULL,
  `permission_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_group_permission_group_id_idx` (`group_id` ASC),
  INDEX `fk_tbl_group_permission_permission_id_idx` (`permission_id` ASC),
  CONSTRAINT `fk_tbl_group_permission_group_id`
    FOREIGN KEY (`group_id`)
    REFERENCES `tbl_group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_group_permission_permission_id`
    FOREIGN KEY (`permission_id`)
    REFERENCES `tbl_permission` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


create TABLE `tbl_province` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(6) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  `short_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


create TABLE `tbl_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `address` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `location_long` float DEFAULT NULL,
  `location_lat` float DEFAULT NULL,
  `ops_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ops_mid` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ops_tid` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`),
  KEY `fk_tbl_city_province_id_idx` (`province_id`),
  CONSTRAINT `fk_tbl_city_province_id` FOREIGN KEY (`province_id`) REFERENCES `tbl_province` (`id`) ON delete NO ACTION ON update NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


CREATE TABLE `tbl_building` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `city_id` INT NULL,
  `name` VARCHAR(256) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_building_city_id_idx` (`city_id` ASC),
  CONSTRAINT `fk_tbl_building_city_id`
    FOREIGN KEY (`city_id`)
    REFERENCES `tbl_city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



CREATE TABLE `tbl_floor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `building_id` INT NULL,
  `name` VARCHAR(256) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_floor_building_id_idx` (`building_id` ASC),
  CONSTRAINT `fk_tbl_floor_building_id`
    FOREIGN KEY (`building_id`)
    REFERENCES `tbl_building` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

  create TABLE `tbl_apartment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` CHAR(32) NULL,
  `city_id` INT NULL,
  `description` VARCHAR(256) NULL,
  `floor_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_apartment_city_id_idx` (`city_id` ASC),
  INDEX `fk_tbl_apartment_floor_id_idx` (`floor_id` ASC),
  CONSTRAINT `fk_tbl_apartment_city_id`
    FOREIGN KEY (`city_id`)
    REFERENCES `tbl_city` (`id`)
    ON delete NO ACTION
    ON update NO ACTION,
  CONSTRAINT `fk_tbl_apartment_floor_id`
    FOREIGN KEY (`floor_id`)
    REFERENCES `tbl_floor` (`id`)
    ON delete NO ACTION
    ON update NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `tbl_user_apartment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` CHAR(16) NULL,
  `apartment_id` INT NULL,
  `group_id` INT NULL,
  `phone_no` VARCHAR(16) NULL,
  `create_date` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_user_apartment_group_id_idx` (`group_id` ASC),
  INDEX `fk_tbl_user_apartment_user_id_idx` (`user_id` ASC),
  INDEX `fk_tbl_user_apartment_apartment_id_idx` (`apartment_id` ASC),
  CONSTRAINT `fk_tbl_user_apartment_group_id`
    FOREIGN KEY (`group_id`)
    REFERENCES `tbl_group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_user_apartment_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `tbl_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_user_apartment_apartment_id`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `tbl_apartment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


create TABLE `tbl_debit_note` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` CHAR(32) NULL,
  `period` VARCHAR(32) NULL,
  `total_amount` DECIMAL(12,4) NULL,
  `owed_amount` DECIMAL(12,4) NULL,
  `paid_amount` DECIMAL(12,4) NULL,
  `currency` CHAR(3) NULL,
  `create_date` TIMESTAMP NULL,
  `due_date` TIMESTAMP NULL,
  `paid_date` TIMESTAMP NULL,
  `paid_by` VARCHAR(32) NULL,
  `apartment_id` INT NULL,
  `status` INT DEFAULT 0,
  `apartment_code` CHAR(32) NULL,
  `city_code` CHAR(32) NULL,
  `display` VARCHAR(256) NULL,
  `ymd` INT NULL,
  `source_pay` INT NULL,
  `description` VARCHAR(255),
  `payment_transaction_id` VARCHAR(32),
  `payment_transaction_ref` VARCHAR(32),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC),
  INDEX `fk_tbl_debit_note_apartment_id_idx` (`apartment_id` ASC),
  CONSTRAINT `fk_tbl_debit_note_apartment_id`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `tbl_apartment` (`id`)
    ON delete NO ACTION
    ON update NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


create TABLE `tbl_folio` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` CHAR(32) NULL,
  `status` INT NULL,
  `create_date` TIMESTAMP NULL,
  `description` VARCHAR(256) NULL,
  `from_date` VARCHAR(256) NULL,
  `to_date` VARCHAR(256) NULL,
  `start_index` VARCHAR(256) NULL,
  `end_index` VARCHAR(256) NULL,
  `quantity` VARCHAR(256) NULL,
  `price` VARCHAR(256) NULL,
  `vat` VARCHAR(256) NULL,
  `vat_amount` VARCHAR(256) NULL,
  `amount` VARCHAR(256) NULL,
  `debit_note_id` INT NOT NULL,
  `folio_detail_id` VARCHAR(256) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_folio_debit_note_id_idx` (`debit_note_id` ASC),
  CONSTRAINT `fk_tbl_folio_debit_note_id`
    FOREIGN KEY (`debit_note_id`)
    REFERENCES `tbl_debit_note` (`id`)
    ON delete NO ACTION
    ON update NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


create TABLE `tbl_work_order_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` CHAR(16) NULL,
  `description` VARCHAR(256) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



create TABLE `tbl_work_order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` CHAR(32) NULL,
  `status` CHAR(16) NULL,
  `create_date` TIMESTAMP NULL,
  `ymd` INT NULL,
  `apartment_id` INT NULL,
  `work_order_type_id` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC),
  INDEX `fk_tbl_work_order_apartment_id_idx` (`apartment_id` ASC),
  INDEX `fk_tbl_work_order_type_id_idx` (`work_order_type_id` ASC),
  CONSTRAINT `fk_tbl_work_order_apartment_id`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `tbl_apartment` (`id`)
    ON delete NO ACTION
    ON update NO ACTION,
  CONSTRAINT `fk_tbl_work_order_type_id`
    FOREIGN KEY (`work_order_type_id`)
    REFERENCES `tbl_work_order_type` (`id`)
    ON delete NO ACTION
    ON update NO ACTION);


create TABLE `tbl_work_order_item` (
  `id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(64) NULL,
  `value` TEXT NULL,
  `description` VARCHAR(256) NULL,
  `work_order_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_work_order_item_work_order_id_idx` (`work_order_id` ASC),
  CONSTRAINT `fk_tbl_work_order_item_work_order_id`
    FOREIGN KEY (`work_order_id`)
    REFERENCES `tbl_work_order` (`id`)
    ON delete NO ACTION
    ON update NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


create TABLE `tbl_payment_transaction` (
  `id` BIGINT NOT NULL,
  `order_ref` CHAR(32) NULL,
  `amount` DECIMAL(12,4) NULL,
  `trans_date` TIMESTAMP NULL,
  `status` CHAR(16) NULL,
  `payer_id` VARCHAR(48) NULL,
  `payer_phone_no` VARCHAR(12) NULL,
  `merchant_code` VARCHAR(32) NULL,
  `merchant_user_id` VARCHAR(32) NULL,
  `description` VARCHAR(256) NULL,
  `device_id` VARCHAR(64) NULL,
  `ip_address` VARCHAR(32) NULL,
  `location_long` VARCHAR(12) NULL,
  `location_lat` VARCHAR(12) NULL,
  `channel_code` VARCHAR(255) NULL,
  `payment_transaction_id` VARCHAR(32) NULL,
  `payment_transaction_ref` VARCHAR(32) NULL,
  `payment_timestamp` BIGINT NULL,
  `payment_detail` VARCHAR(512) NULL,
  `payment_secure_token` VARCHAR(512) NULL,
  `tbl_payment_transactioncol` INT NULL,
  `mid` VARCHAR(32) NULL,
  `tid` VARCHAR(32) NULL,
  `payment_callback_url` VARCHAR(512) NULL,
  `version` INT(11) DEFAULT 0,
  `ymd` INT(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


create TABLE `tbl_request_log` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `request_id` VARCHAR(64) NULL,
  `device_id` VARCHAR(64) NULL,
  `device_session_id` VARCHAR(64) NULL,
  `device_type` VARCHAR(64) NULL,
  `client_type` VARCHAR(64) NULL,
  `client_version` VARCHAR(64) NULL,
  `access_token` VARCHAR(512) NULL,
  `user_id` VARCHAR(64) NULL,
  `user_id_type` VARCHAR(64) NULL,
  `accept_language` VARCHAR(8) NULL,
  `request_data` TEXT NULL,
  `response_data` TEXT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

create TABLE `tbl_config` (
   `id` INT NOT NULL AUTO_INCREMENT,
   `key` VARCHAR(64) NOT NULL unique,
   `value` VARCHAR(4096) NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;




create table tbl_sequence
(
	seq_name varchar(50) not null primary key,
	seq_val int unsigned not null
);

drop procedure if exists getNextSeq;
DELIMITER $$
create procedure getNextSeq(IN sSeqName varchar(100),  OUT value int)
begin
    START TRANSACTION;
      INSERT INTO tbl_sequence(seq_name, seq_val) values(sSeqName, 1)
      on duplicate key UPDATE  seq_val = seq_val + 1;
      SELECT seq_val INTO value FROM tbl_sequence WHERE seq_name = sSeqName;
    COMMIT;
end$$
DELIMITER ;
