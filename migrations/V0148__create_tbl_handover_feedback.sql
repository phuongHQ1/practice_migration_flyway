CREATE TABLE `tbl_handover_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apartment_id` int(11) NOT NULL,
  `title` varchar(1024) COLLATE utf8mb4_bin NOT NULL,
  `status` int(1) DEFAULT '0',
  `description` varchar(2048) COLLATE utf8mb4_bin NOT NULL,
  `sap_equipment_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `sap_equipment_name` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL,
  `sap_ticket_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `sap_ticket_status` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_apartment_id_idx` (`apartment_id`),
  CONSTRAINT `fk_tbl_apartment_id` FOREIGN KEY (`apartment_id`) REFERENCES `tbl_apartment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;