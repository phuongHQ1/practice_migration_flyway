ALTER TABLE `tbl_user_bill_reminder` add COLUMN `apartment_code` varchar(128);
ALTER TABLE `tbl_user_bill_reminder` change COLUMN `remider_id` `reminder_id` char(16);
ALTER TABLE `tbl_user_bill_reminder` change COLUMN `remider_time` `reminder_time` timestamp null default null;