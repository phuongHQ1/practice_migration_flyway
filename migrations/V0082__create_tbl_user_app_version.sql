CREATE TABLE `tbl_user_app_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(16) DEFAULT NULL,
  `app_version` varchar(20) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `device_type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

