ALTER TABLE `tbl_debit_note`
ADD COLUMN `city_id` INT NULL AFTER `city_code`;

ALTER TABLE `tbl_debit_note`
ADD COLUMN `internal_transaction_id` BIGINT NULL AFTER `city_id`;

