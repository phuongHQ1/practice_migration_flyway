CREATE TABLE `tbl_logging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_ip` varchar(128) NOT NULL,
  `create_date` VARCHAR(20) NOT NULL,
  `create_user` VARCHAR(20) NOT NULL,
  `method` varchar(20) NOT NULL,
  `params` varchar(512),
  `request_body` longtext ,
  `response_body` longtext NOT NULL,
  `status_code` varchar(20) DEFAULT NULL,
  `uri` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
