CREATE TABLE `tbl_utility_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL COMMENT 'SAP utility code',
  `description` varchar(45) DEFAULT NULL,
  `icon_url` varchar(512) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1: Active\n0: Inactive\n',
  `created_date` timestamp NULL DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
