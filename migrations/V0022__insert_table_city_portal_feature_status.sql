START TRANSACTION;

CREATE TABLE `tbl_portal_feature` (
    `id` int NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(200) NOT NULL,
    `display_name` VARCHAR(200) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (code),
    UNIQUE (display_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

INSERT
    INTO `tbl_portal_feature` (code, display_name)
VALUES ("FEEDBACK", "Phản Ánh & Góp Ý"),
       ("SERVICE", "Dịch Vụ"),
       ("ULTILITIES", "Tiện Ích"),
       ("NOTIFICATION", "Thông Báo"),
       ("NEWS", "Tin Tức"),
       ("PROJECTS", "Thông Tin Dự Án"),
       ("HANDBOOKS", "Cẩm Nang"),
       ("PHONEBOOKS", "Danh Bạ"),
       ("ACCOUNT_MANAGEMENT", "Quản Lý Tài Khoản"),
       ("CATEGORY_MANAGEMENT", "Quản Lý Danh Mục"),
       ("TERM_AND_CONDITION_MANAGEMENT", "Điều Khoản & Điều Kiện")
;


CREATE TABLE `tbl_city_portal_feature_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `feature_id` int NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  CONSTRAINT unique_city_feature UNIQUE (city_id, feature_id),
  CONSTRAINT `fk_to_tbl_city` FOREIGN KEY (`city_id`) REFERENCES `tbl_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_to_tbl_portal_feature` FOREIGN KEY (`feature_id`) REFERENCES `tbl_portal_feature` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

COMMIT;
