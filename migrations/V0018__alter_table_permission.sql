UPDATE `tbl_permission` SET `description`='Thông tin người dùng' WHERE `id`='10';
UPDATE `tbl_permission` SET `description`='Hồ sơ thông tin' WHERE `id`='11';
UPDATE `tbl_permission` SET `description`='Quản lí thành viên' WHERE `id`='12';
UPDATE `tbl_permission` SET `description`='Quản lí thẻ' WHERE `id`='13';
UPDATE `tbl_permission` SET `description`='Đăng kí tiện ích' WHERE `id`='20';
UPDATE `tbl_permission` SET `description`='Đăng kí thẻ cư dân' WHERE `id`='21';
UPDATE `tbl_permission` SET `description`='Đăng kí thẻ xe' WHERE `id`='22';
UPDATE `tbl_permission` SET `description`='Đăng kí thẻ ra vào' WHERE `id`='23';
UPDATE `tbl_permission` SET `description`='Đăng kí chuyển đồ' WHERE `id`='24';
UPDATE `tbl_permission` SET `description`='Đăng kí khách lên nhà' WHERE `id`='25';
UPDATE `tbl_permission` SET `description`='Đăng kí sạc xe' WHERE `id`='26';
UPDATE `tbl_permission` SET `description`='Dịch vụ tiện ích' WHERE `id`='30';
UPDATE `tbl_permission` SET `description`='Thanh toán hóa đơn' WHERE `id`='40';
UPDATE `tbl_permission` SET `description`='Nhắc nhở' WHERE `id`='50';
UPDATE `tbl_permission` SET `description`='Dịch vụ liên kết' WHERE `id`='60';
UPDATE `tbl_permission` SET `description`='Thông báo cư dân' WHERE `id`='70';
UPDATE `tbl_permission` SET `description`='Phản ánh' WHERE `id`='80';
UPDATE `tbl_permission` SET `description`='Voucher' WHERE `id`='90';
UPDATE `tbl_permission` SET `description`='Cẩm nang cư dân' WHERE `id`='100';
UPDATE `tbl_permission` SET `description`='Thông tin dự án' WHERE `id`='110';
UPDATE `tbl_permission` SET `description`='Tin tức' WHERE `id`='120';
UPDATE `tbl_permission` SET `description`='Onboarding' WHERE `id`='130';
