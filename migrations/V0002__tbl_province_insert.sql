insert into `tbl_province` (`id`, `code`, `name`, `short_name`, `display_order`) VALUES ('1', 'HCM', 'TP Hồ Chí Minh', 'TP HCM', '1');
insert into `tbl_province` (`id`, `code`, `name`, `short_name`, `display_order`) VALUES ('2', 'HN', 'TP Hà Nội', 'TP Hà Nội', '2');
insert into `tbl_province` (`id`, `code`, `name`, `short_name`, `display_order`) VALUES ('3', 'DN', 'Đà Nẵng', 'Đà Nẵng', '3');
