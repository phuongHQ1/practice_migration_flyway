CREATE TABLE `tbl_building_service_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `building_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`building_id`, `service_type_id`),
  KEY `fk_tbl_building_service_type_building_id` (`building_id`),
  KEY `fk_tbl_building_service_type_service_type_id` (`service_type_id`),
  CONSTRAINT `fk_tbl_building_service_type_building_id` FOREIGN KEY (`building_id`) REFERENCES tbl_building(`id`),
  CONSTRAINT `fk_tbl_building_service_type_service_type_id` FOREIGN KEY (`service_type_id`) REFERENCES tbl_service_type(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;