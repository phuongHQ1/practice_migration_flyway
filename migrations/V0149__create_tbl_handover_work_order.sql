CREATE TABLE `tbl_handover_work_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `apartment_id` int(11) NOT NULL,
  `sap_workorder_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_handover_workorder_apartment_id_idx` (`apartment_id`),
  CONSTRAINT `fk_tbl_handover_workorder_apartment_id_idx` FOREIGN KEY (`apartment_id`) REFERENCES `tbl_apartment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;