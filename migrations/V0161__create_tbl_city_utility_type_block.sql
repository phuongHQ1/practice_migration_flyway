CREATE TABLE `tbl_city_utility_type_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blocking_day` int(11) DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `fault_number` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0: unactive, 1: active',
  `updated_by` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `utility_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK23r15ssc82g6omen4hg0r0tth` (`city_id`),
  KEY `FKgr9ia4br0tben6f75qw1u086` (`utility_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
