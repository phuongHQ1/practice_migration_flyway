CREATE TABLE `idhomes_service`.`tbl_user_approve`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(16) NOT NULL,
  `apartment_id` int(11) NULL,
  `apartment_code` varchar(128) NULL,
  `created_date` timestamp(0) NULL,
  `created_by` varchar(32) NULL,
  `updated_date` timestamp(0) NULL,
  `updated_by` varchar(32) NULL,
  `status` int(11) NULL,
  PRIMARY KEY (`id`)
);