CREATE TABLE `tbl_third_party_city_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `fee` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` int(3) DEFAULT '1' COMMENT '1 for active, 0 for inactive',
  `updated_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `third_party_service_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_third_party_city_service_city_id_idx` (`city_id`),
  KEY `fk_tbl_third_party_city_service_third_party_service_id_idx` (`third_party_service_id`),
  CONSTRAINT `fk_tbl_third_party_city_service_city_id_idx` FOREIGN KEY (`city_id`) REFERENCES `tbl_city` (`id`),
  CONSTRAINT `fk_tbl_third_party_city_service_third_party_service_id_idx` FOREIGN KEY (`third_party_service_id`) REFERENCES `tbl_third_party_service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;