
INSERT INTO `tbl_utility_type` (`id`, `code`, `description`, `icon_url`, `status`, `created_date`, `created_by`, `updated_by`, `display_order`)
VALUES ('1', '0001', 'Sân BBQ', 'https://freelogovector.net/wp-content/uploads/logo-images-14/vingroup-logo-vector-91640.png', '1', NOW(), 'admin', '', '1');

INSERT INTO `tbl_utility_type` (`id`, `code`, `description`, `icon_url`, `status`, `created_date`, `created_by`, `updated_by`, `display_order`)
VALUES ('2', '0002', 'Sân Tennis', 'https://freelogovector.net/wp-content/uploads/logo-images-14/vingroup-logo-vector-91640.png', '1', NOW(), 'admin', '', '2');

INSERT INTO `tbl_utility_type` (`id`, `code`, `description`, `icon_url`, `status`, `created_date`, `created_by`, `updated_by`, `display_order`)
VALUES ('3', '0003', 'Hồ bơi', 'https://freelogovector.net/wp-content/uploads/logo-images-14/vingroup-logo-vector-91640.png', '1', NOW(), 'admin', '', '3');

INSERT INTO `tbl_utility_type` (`id`, `code`, `description`, `icon_url`, `status`, `created_date`, `created_by`, `updated_by`, `display_order`)
VALUES ('4', '0004', 'Sân đá bóng', 'https://freelogovector.net/wp-content/uploads/logo-images-14/vingroup-logo-vector-91640.png', '1', NOW(), 'admin', '', '4');

INSERT INTO `tbl_utility_type` (`id`, `code`, `description`, `icon_url`, `status`, `created_date`, `created_by`, `updated_by`, `display_order`)
VALUES ('5', '0005', 'Sân bóng rổ', 'https://freelogovector.net/wp-content/uploads/logo-images-14/vingroup-logo-vector-91640.png', '1', NOW(), 'admin', '', '5');


UPDATE `tbl_utility_type` SET `icon_url` = 'https://www.googleapis.com/download/storage/v1/b/vinid-idhomes-public-nonprod/o/icBasketball@3x-1e4550a8-ea5a-4cc7-824d-b461b260382d.png?generation=1560489974919742&alt=media' WHERE (`id` = '5');
UPDATE `tbl_utility_type` SET `icon_url` = 'https://www.googleapis.com/download/storage/v1/b/vinid-idhomes-public-nonprod/o/icBbq@3x-a5cec399-2c55-4f8e-9f3f-d41e9fe2928c.png?generation=1560490420637882&alt=media' WHERE (`id` = '1');
UPDATE `tbl_utility_type` SET `icon_url` = 'https://www.googleapis.com/download/storage/v1/b/vinid-idhomes-public-nonprod/o/icFootballStadium@3x-14ad184e-6b9f-4d15-bf89-a54e606f4155.png?generation=1560490457868072&alt=media' WHERE (`id` = '4');
UPDATE `tbl_utility_type` SET `icon_url` = 'https://www.googleapis.com/download/storage/v1/b/vinid-idhomes-public-nonprod/o/icNearmeInformative@3x-e5a87983-e9b0-4d83-a345-3049fafd3626.png?generation=1560490501632617&alt=media' WHERE (`id` = '2');
UPDATE `tbl_utility_type` SET `icon_url` = 'https://www.googleapis.com/download/storage/v1/b/vinid-idhomes-public-nonprod/o/icNearmeInformative@3x-609e59a2-ebc7-4982-a3ca-16906be5110c.png?generation=1560490566534298&alt=media' WHERE (`id` = '3');
