ALTER TABLE tbl_city_service_type
add COLUMN created_by varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
add COLUMN created_date datetime DEFAULT NULL,
add COLUMN updated_by varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
add COLUMN updated_date datetime DEFAULT NULL,
add COLUMN fee varchar(255) COLLATE utf8mb4_bin DEFAULT NULL;
