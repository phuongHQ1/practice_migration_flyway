CREATE TABLE `tbl_referrer`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(16) COLLATE utf8mb4_bin NULL,
  `apartment_id` int(11) NULL,
  `phone_no` varchar(128) NULL,
  `referrer_code` varchar(128) NULL,
  `created_date` timestamp(0) NULL,
  `created_ymd` int(11) NULL,
  PRIMARY KEY (`id`),
CONSTRAINT `fk_tbl_referrer_user_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
CONSTRAINT `fk_tbl_referrer_apartment_id` FOREIGN KEY (`apartment_id`) REFERENCES `tbl_apartment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
