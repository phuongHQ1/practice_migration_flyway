CREATE TABLE `tbl_user_apartment_history` (
  `id` int(11) NOT NULL,
  `user_id` char(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `apartment_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `phone_no` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `resident_id` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1: active\n2: another acc sync',
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin