INSERT INTO `tbl_apartment`(`status`,`code`,`name`,`description`,`city_id`,`sap_block_id`,`sap_floor_id`,`floor_id`)
VALUES ('10','ID_BGN','24VHOP','ID_BGN','19','23','501',(SELECT `id` FROM `tbl_floor` where `sap_area_id` = '19' AND `sap_block_id` = '23' AND `sap_floor_id` = '501' ));

INSERT INTO `tbl_handover_apartment`(`created_at`,`updated_at`,`status`,`city_id`,`apartment_id`)
SELECT NOW(),NOW(),1,19,`id` FROM `tbl_apartment` WHERE `city_id` = 19 AND `code` = 'ID_BGN';