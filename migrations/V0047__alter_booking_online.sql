ALTER TABLE `tbl_booking_online`
ADD COLUMN `sap_ticket_id` VARCHAR(32) NULL AFTER `requestor_name`,
ADD COLUMN `sap_ticket_status` VARCHAR(32) NULL AFTER `sap_ticket_id`;
