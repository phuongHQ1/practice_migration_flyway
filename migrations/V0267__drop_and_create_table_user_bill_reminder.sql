DROP TABLE IF EXISTS `tbl_user_bill_reminder`;

CREATE TABLE `tbl_user_bill_reminder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` char(16) DEFAULT NULL,
  `customer_code` varchar(45) DEFAULT NULL,
  `provider_id` char(16) DEFAULT NULL,
  `service_id` char(16) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `reminder_id` char(16) DEFAULT NULL,
  `reminder_time` timestamp NULL DEFAULT NULL,
  `reminder_exp_time` timestamp NULL DEFAULT NULL,
  `user_approval_status` tinyint(4) DEFAULT NULL,
  `user_approval_time` timestamp NULL DEFAULT NULL,
  `apartment_code` varchar(128) DEFAULT NULL,
  `created_yymmdd` varchar(45) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`user_id`,`customer_code`)
) ENGINE=InnoDB;