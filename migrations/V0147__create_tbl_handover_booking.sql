CREATE TABLE `tbl_handover_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apartment_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `booking_date` datetime DEFAULT NULL,
  `booking_hour` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `booking_ymd` int(11) DEFAULT NULL,
  `building_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `handover_worker` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `name_search` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;