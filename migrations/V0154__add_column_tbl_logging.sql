ALTER TABLE `idhomes_service`.`tbl_logging`
ADD COLUMN `authorization` longtext COLLATE utf8mb4_bin,
ADD COLUMN   `uuid` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
ADD COLUMN  `browser_info` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL;


