CREATE TABLE `idhomes_service`.`tbl_apartment_status`  (
  `id` int(11) NOT NULL,
  `code` varchar(64) NULL,
  `description` varchar(128) NULL,
  PRIMARY KEY (`id`)
);