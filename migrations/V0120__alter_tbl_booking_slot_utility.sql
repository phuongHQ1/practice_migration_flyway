alter table tbl_booking_slot_utility  
  add column `portal_status` int(11) DEFAULT NULL,
  add column `portal_comment` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  add column `portal_updated_date` timestamp NULL DEFAULT NULL,
  add column `portal_updated_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL;
  
