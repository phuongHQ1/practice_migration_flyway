CREATE TABLE `idhomes_service`.`tbl_apartment_status_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apartment_status_id` int(11) NULL,
  `permission_id` int(11) NULL,
  `city_id` int(11) NULL,
  PRIMARY KEY (`id`)
);