CREATE TABLE `idhomes_service`.`tbl_relationship` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sap_id` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `status` INT NULL DEFAULT 1,
  `display_order` INT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
