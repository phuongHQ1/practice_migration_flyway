CREATE TABLE `tbl_residentnews_approval` (
  `id` int(11) NOT NULL,
  `residentnews_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `status_id` int(11) DEFAULT NULL,
  `approver` varchar(45) DEFAULT NULL,
  `sent_time` timestamp NULL DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `building_name` varchar(255) DEFAULT NULL,
  `floor_name` varchar(255) DEFAULT NULL,
  `apartment_code` varchar(45) DEFAULT NULL,
  `approval` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
