alter table tbl_city_utility_type add column min_adult int(11) after limit_adult;
alter table tbl_city_utility_type add column min_child int(11) after limit_child;
alter table tbl_city_utility_type add column start_time_booking float after open_hour;
alter table tbl_city_utility_type add column end_time_booking float after close_hour;