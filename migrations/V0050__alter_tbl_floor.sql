ALTER TABLE `tbl_floor` 
ADD COLUMN `sap_floor_id` INT(11) NOT NULL AFTER `building_id`,
ADD COLUMN `sap_area_id` INT(11) NOT NULL AFTER `sap_floor_id`,
ADD COLUMN `sap_block_id` INT(11) NOT NULL AFTER `sap_area_id`;