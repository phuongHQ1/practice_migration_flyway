ALTER TABLE `idhomes_service`.`tbl_service_type`
ADD COLUMN `status` INT(3) DEFAULT 1 COMMENT '1 for active, 0 for inactive';
