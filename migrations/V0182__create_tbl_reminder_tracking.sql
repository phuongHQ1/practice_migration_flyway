CREATE TABLE `tbl_reminder_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apartment_id` int(11) DEFAULT NULL,
  `reference_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `reminder_id` int(11) DEFAULT NULL,
  `reminder_type` int(3) DEFAULT NULL COMMENT '1 for booking utility, 2 for debit note',
  `updated_by` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
