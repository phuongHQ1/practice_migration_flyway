CREATE TABLE `tbl_booking_utility_inst` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `booking_ymd` INT NOT NULL,
  `booking_slot` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UNIQUE_ymd_slot` (`booking_ymd` ASC, `booking_slot` ASC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
