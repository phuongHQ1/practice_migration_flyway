CREATE TABLE `idhomes_service`.`tbl_building_utility`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `building_id` int(11) NOT NULL,
  `utility_id` int(11) NOT NULL,
  `status` int(11) NULL,
  `created_date` timestamp(0) NULL,
  `updated_date` timestamp(0) NULL,
  PRIMARY KEY (`id`)
);