CREATE TABLE `tbl_city_service_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT '1',
  `city_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_city_service_type_type_id_idx` (`service_type_id`),
  CONSTRAINT `fk_tbl_city_service_type_service_type_id` FOREIGN KEY (`service_type_id`) REFERENCES `tbl_service_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
